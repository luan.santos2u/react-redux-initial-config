import { Reducer } from 'redux'

import { UserState, UserTypes } from './types'

const INITIAL_STATE: UserState = {
  hello: "Real2U",
  name: "Luan"
}

const reducer: Reducer<UserState> = (state = INITIAL_STATE, action) => {
  switch(action.type) {
    case UserTypes.SET_USER_NAME:
      return {...state, name: action.payload.name}
    default:
      return state
  }
}

export default reducer