import React from 'react';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom'
import { Provider } from 'react-redux'

import User from './User'

import './config/ReactotronConfig'

import { store } from './store'

function App() {
  return (
    <Provider store={store}>
      <Router>
        <Switch>
          <Route component={User} exact path="/"/>
        </Switch>
      </Router>
    </Provider>
  );
}

export default App;
